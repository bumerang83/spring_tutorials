/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package event.handling.in.spring;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/event_handling_in_spring.htm
public class CStopEventHandler implements ApplicationListener<ContextStoppedEvent> {

    /**
     *
     * @param event
     */
    @Override
   public void onApplicationEvent(ContextStoppedEvent event) {
      System.out.println("ContextStoppedEvent Received");
   }    
}
