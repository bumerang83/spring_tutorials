/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.jdbc.example;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_jdbc_example.htm
// added additional netbean 8.1 library to compile and execute code: ~/netbeans-8.1/ide/modules/ext/mysql-connector-java-5.1.23-bin.jar
public class StudentMapper implements RowMapper<Student> {
   @Override
   public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
      Student student = new Student();
      student.setId(rs.getInt("id"));
      student.setName(rs.getString("name"));
      student.setAge(rs.getInt("age"));
      return student;
   }    
}
