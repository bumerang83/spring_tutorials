/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.logging.with.log4j;

/*
 * @author    : bumerang    
 * Document   : HelloWorld
 * Created on : Apr 24, 2017, 8:12:38 AM
 */
// https://www.tutorialspoint.com/spring/logging_with_log4j.htm
public class HelloWorld {
   private String message;
   
   public void setMessage(String message){
      this.message  = message;
   }
   public void getMessage() {
      System.out.println("Your Message : " + message);
   }
}
