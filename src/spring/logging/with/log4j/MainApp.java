/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.logging.with.log4j;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.apache.log4j.Logger;

// below was taken from the second example
// https://www.tutorialspoint.com/spring/logging_with_log4j.htm
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/*
 * @author    : bumerang    
 * Document   : MainApp
 * Created on : Apr 24, 2017, 8:13:33 AM
 */
// https://www.tutorialspoint.com/spring/logging_with_log4j.htm
public class MainApp {
   // the first example (above, log4j)
//   static Logger log = Logger.getLogger(MainApp.class.getName());
    
   // the second example (below, commons logging)
   static Log log = LogFactory.getLog(MainApp.class.getName());
   
   public static void main(String[] args) {
      ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
      log.info("Going to create HelloWord Obj");
      HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
      obj.getMessage();
      
      log.info("Exiting the program");
   }
}
