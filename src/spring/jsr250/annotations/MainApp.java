/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.jsr250.annotations;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * @author    : bumerang    
 * Document   : MainApp
 * Created on : Mar 29, 2017, 11:26:04 AM
 */
// https://www.tutorialspoint.com/spring/spring_jsr250_annotations.htm
public class MainApp {
   public static void main(String[] args) {
      AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

      HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
      obj.getMessage();
      context.registerShutdownHook();
      
      TextEditor te = (TextEditor) context.getBean("textEditor");
      te.spellCheck();
   }
}
