/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.jsr250.annotations;

import spring.autowired.annotation.setter.methods.*;

/*
 * @author    : bumerang    
 * Document   : SpellChecker
 * Created on : Mar 20, 2017, 11:43:03 AM
 */
// https://www.tutorialspoint.com/spring/spring_jsr250_annotations.htm
public class SpellChecker {
   public SpellChecker(){
      System.out.println("Inside SpellChecker constructor." );
   }
   public void checkSpelling(){
      System.out.println("Inside checkSpelling." );
   }
}
