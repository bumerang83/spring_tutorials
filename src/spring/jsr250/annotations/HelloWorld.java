/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.jsr250.annotations;

import javax.annotation.*;

/*
 * @author    : bumerang    
 * Document   : HelloWorld
 * Created on : Mar 29, 2017, 11:24:43 AM
 */
// https://www.tutorialspoint.com/spring/spring_jsr250_annotations.htm
public class HelloWorld {
   private String message;

   public void setMessage(String message){
      this.message  = message;
   }
   public String getMessage(){
      System.out.println("Your Message : " + message);
      return message;
   }
   
   @PostConstruct
   public void init(){
      System.out.println("Bean is going through init.");
   }
   
   @PreDestroy
   public void destroy(){
      System.out.println("Bean will destroy now.");
   }
}
