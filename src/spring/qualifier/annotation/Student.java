/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.qualifier.annotation;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_qualifier_annotation.htm
public class Student {
   private Integer age;
   private String name;
   
/*   
   public Student(){
      System.out.println("Inside Student constructor." );
   }
*/
   
   public void setAge(Integer age) {
      this.age = age;
   }
   
   public Integer getAge() {
      return age;
   }

   public void setName(String name) {
      this.name = name;
   }
   
   public String getName() {
      return name;
   }    
}
