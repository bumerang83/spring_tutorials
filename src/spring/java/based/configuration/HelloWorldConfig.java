/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.java.based.configuration;

import org.springframework.context.annotation.*;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_java_based_configuration.htm
// @Configuration & @Bean Annotations
@Configuration
public class HelloWorldConfig {
   @Bean 
   public HelloWorld helloWorld(){
      return new HelloWorld();
   }
}
