/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.autowired.annotation.setter.methods;

import org.springframework.beans.factory.annotation.Autowired;

/*
 * @author    : bumerang    
 * Document   : TextEditor
 * Created on : Mar 20, 2017, 11:42:00 AM
 */
// https://www.tutorialspoint.com/spring/spring_autowired_annotation.htm
public class TextEditor {
   private SpellChecker spellChecker;

   @Autowired
   public void setSpellChecker( SpellChecker spellChecker ){
      this.spellChecker = spellChecker;
   }
   public SpellChecker getSpellChecker( ) {
      return spellChecker;
   }
   public void spellCheck() {
      spellChecker.checkSpelling();
   }
}
