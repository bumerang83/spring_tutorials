/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.autowired.annotation.properties;

/*
 * @author    : bumerang    
 * Document   : SpellChecker
 * Created on : Mar 20, 2017, 12:13:30 PM
 */
// https://www.tutorialspoint.com/spring/spring_autowired_annotation.htm
public class SpellChecker {
   public SpellChecker(){
      System.out.println("Inside SpellChecker constructor." );
   }
   public void checkSpelling(){
      System.out.println("Inside checkSpelling." );
   }
}
