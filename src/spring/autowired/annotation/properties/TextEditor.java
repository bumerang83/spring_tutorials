/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.autowired.annotation.properties;

import org.springframework.beans.factory.annotation.Autowired;

/*
 * @author    : bumerang    
 * Document   : TextEditor
 * Created on : Mar 20, 2017, 12:12:21 PM
 */
// https://www.tutorialspoint.com/spring/spring_autowired_annotation.htm
public class TextEditor {
   @Autowired
   private SpellChecker spellChecker;

   public TextEditor() {
      System.out.println("Inside TextEditor constructor." );
   }
   public SpellChecker getSpellChecker( ){
      return spellChecker;
   }
   public void spellCheck(){
      spellChecker.checkSpelling();
   }
}
