/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.autowired.annotation.option;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Autowired;

/*
 * @author    : bumerang    
 * Document   : Student
 * Created on : Mar 22, 2017, 10:45:22 AM
 */
// https://www.tutorialspoint.com/spring/spring_autowired_annotation.htm
public class Student {
   private Integer age;
   private String name;

   @Autowired(required=false)
//   @Required
//   @Autowired
   public void setAge(Integer age) {
      this.age = age;
   }
   public Integer getAge() {
      return age;
   }
   
   @Autowired
//   @Required
   public void setName(String name) {
      this.name = name;
   }
   public String getName() {
      return name;
   }
}
