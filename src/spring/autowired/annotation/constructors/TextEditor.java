/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.autowired.annotation.constructors;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_autowired_annotation.htm
public class TextEditor {
   private SpellChecker spellChecker;

   @Autowired
   public TextEditor(SpellChecker spellChecker){
      System.out.println("Inside TextEditor constructor." );
      this.spellChecker = spellChecker;
   }

   public void spellCheck(){
      spellChecker.checkSpelling();
   }    
}
