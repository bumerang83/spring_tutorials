/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.autowiring.byconstructor;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_autowiring_byconstructor.htm
public class TextEditor {
   private SpellChecker spellChecker;
   private String name;

   public TextEditor( SpellChecker spellChecker, String name ) {
      this.spellChecker = spellChecker;
      this.name = name;
   }
   public SpellChecker getSpellChecker() {
      return spellChecker;
   }
   public String getName() {
      return name;
   }

   public void spellCheck() {
      spellChecker.checkSpelling();
   }    
}
