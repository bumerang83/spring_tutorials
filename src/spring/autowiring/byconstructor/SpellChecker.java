/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.autowiring.byconstructor;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_autowiring_byconstructor.htm
public class SpellChecker {
   public SpellChecker(){
      System.out.println("Inside SpellChecker constructor." );
   }

   public void checkSpelling()
   {
      System.out.println("Inside checkSpelling." );
   }    
}
