/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.declarative.transaction.management;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * Author: bumerrrang
 * 
 * File:   StudentJDBCTemplate.java 
 *
 * Created on: 29-Nov-2016, 09:58:18
 *
 * Project name: HelloSpring
 *
 */
// https://www.tutorialspoint.com/spring/declarative_management.htm

/*
/*
CREATE TABLE Student(
   ID   INT NOT NULL AUTO_INCREMENT,
   NAME VARCHAR(20) NOT NULL,
   AGE  INT NOT NULL,
   PRIMARY KEY (ID)
);

CREATE TABLE Marks(
   SID INT NOT NULL,
   MARKS  INT NOT NULL,
   YEAR   INT NOT NULL
);
*/

public class StudentJDBCTemplate implements StudentDAO {
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setDataSource(DataSource dataSource) {
      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    
    @Override
    public void create(String name, Integer age, Integer marks, Integer year){

        try {
           String SQL1 = "insert into Student (name, age) values (?, ?)";
           jdbcTemplateObject.update( SQL1, name, age);

           // Get the latest student id to be used in Marks table
           String SQL2 = "select max(id) from Student";           
//         int sid = jdbcTemplateObject.queryForInt( SQL2 );
//          queryForInt() method was deprecated since Spring 3.2.2
// http://stackoverflow.com/questions/15661313/jdbctemplate-queryforint-long-is-deprecated-in-spring-3-2-2-what-should-it-be-r
// changed it to 'queryForInt( SQL2 ) -> queryForObject(SQL2, Integer.class)
           int sid = jdbcTemplateObject.queryForObject(SQL2, Integer.class);

           String SQL3 = "insert into Marks(sid, marks, year) " + 
                         "values (?, ?, ?)";
           jdbcTemplateObject.update( SQL3, sid, marks, year);

           System.out.println("Created Name = " + name + ", Age = " + age);
           // to simulate the exception.
           throw new RuntimeException("simulate Error condition") ;
        } catch (DataAccessException e) {
           System.out.println("Error in creating record, rolling back");
           throw e;
        }
    }
    
    @Override
    public List<StudentMarks> listStudents() {
        String SQL = "select * from Student, Marks where Student.id=Marks.sid";

        List <StudentMarks> studentMarks=jdbcTemplateObject.query(SQL, 
        new StudentMarksMapper());
        return studentMarks;
    }
}
