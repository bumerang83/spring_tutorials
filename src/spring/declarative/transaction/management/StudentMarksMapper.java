/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package spring.declarative.transaction.management;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * Author: bumerrrang
 * 
 * File:   StudentMarksMapper.java 
 *
 * Created on: 29-Nov-2016, 09:55:53
 *
 * Project name: HelloSpring
 *
 */
// https://www.tutorialspoint.com/spring/declarative_management.htm

/*
/*
CREATE TABLE Student(
   ID   INT NOT NULL AUTO_INCREMENT,
   NAME VARCHAR(20) NOT NULL,
   AGE  INT NOT NULL,
   PRIMARY KEY (ID)
);

CREATE TABLE Marks(
   SID INT NOT NULL,
   MARKS  INT NOT NULL,
   YEAR   INT NOT NULL
);
*/

public class StudentMarksMapper implements RowMapper<StudentMarks> {
   @Override
   public StudentMarks mapRow(ResultSet rs, int rowNum) throws SQLException {

      StudentMarks studentMarks = new StudentMarks();

      studentMarks.setId(rs.getInt("id"));
      studentMarks.setName(rs.getString("name"));
      studentMarks.setAge(rs.getInt("age"));
      studentMarks.setSid(rs.getInt("sid"));
      studentMarks.setMarks(rs.getInt("marks"));
      studentMarks.setYear(rs.getInt("year"));

      return studentMarks;
   }
}
