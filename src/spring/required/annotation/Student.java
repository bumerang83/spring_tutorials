/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.required.annotation;

import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_required_annotation.htm
public class Student {
   private Integer age;
   private String name;

   @Required
   public void setAge(Integer age) {
      this.age = age;
   }
   public Integer getAge() {
      return age;
   }

   @Required
   public void setName(String name) {
      this.name = name;
   }
   public String getName() {
      return name;
   }    
}
