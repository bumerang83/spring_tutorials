/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resource.annotation;

import javax.annotation.*;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_jsr250_annotations.htm
public class TextEditor {
   private SpellChecker spellChecker;

   @Resource(name= "spellChecker")
   public void setSpellChecker( SpellChecker spellChecker ){
      this.spellChecker = spellChecker;
   }
   public SpellChecker getSpellChecker(){
      return spellChecker;
   }
   public void spellCheck(){
      spellChecker.checkSpelling();
   }    
}
