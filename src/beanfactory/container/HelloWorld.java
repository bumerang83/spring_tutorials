/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beanfactory.container;

/*
 * @author    : bumerang    
 * Document   : HelloWorld
 * Created on : Mar 1, 2017, 4:49:15 PM
 */
// https://www.tutorialspoint.com/spring/spring_beanfactory_container.htm
public class HelloWorld {
   private String message;

   public void setMessage(String message){
      this.message  = message;
   }

   public void getMessage(){
      System.out.println("Your Message : " + message);
   }
}
