/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom.events.in.spring;

import org.springframework.context.ApplicationEvent;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/custom_events_in_spring.htm
public class CustomEvent extends ApplicationEvent {
   public CustomEvent(Object source) {
      super(source);
   }
   
   @Override
   public String toString(){
      return "My Custom Event";
   }   
}
