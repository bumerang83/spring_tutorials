/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package custom.events.in.spring;

import org.springframework.context.ApplicationListener;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/custom_events_in_spring.htm
public class CustomEventHandler implements ApplicationListener<CustomEvent> {

    /**
     *
     * @param event
     */
    @Override
   public void onApplicationEvent(CustomEvent event) {
      System.out.println(event.toString());
   }    
}
