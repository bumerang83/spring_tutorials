/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package injecting.bean.dependencies;

import org.springframework.context.annotation.*;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/spring_java_based_configuration.htm
@Configuration
public class TextEditorConfig {
   @Bean 
   public TextEditor textEditor(){
      return new TextEditor( spellChecker() );
   }

   @Bean 
   public SpellChecker spellChecker(){
      return new SpellChecker( );
   }
}
