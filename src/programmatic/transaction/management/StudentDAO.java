/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programmatic.transaction.management;

import java.util.List;
import javax.sql.DataSource;

/**
 *
 * Author: bumerrrang
 * 
 * File:   StudentDAO.java 
 *
 * Created on: 22-Nov-2016, 09:02:57
 *
 * Project name: HelloSpring
 *
 */
// https://www.tutorialspoint.com/spring/programmatic_management.htm
/*
CREATE TABLE Student(
   ID   INT NOT NULL AUTO_INCREMENT,
   NAME VARCHAR(20) NOT NULL,
   AGE  INT NOT NULL,
   PRIMARY KEY (ID)
);

CREATE TABLE Marks(
   SID INT NOT NULL,
   MARKS  INT NOT NULL,
   YEAR   INT NOT NULL
);
*/
public interface StudentDAO {
   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
     * @param ds
    */
   public void setDataSource(DataSource ds);
   /** 
    * This is the method to be used to create
    * a record in the Student and Marks tables.
     * @param name
     * @param age
     * @param marks
     * @param year
    */
   public void create(String name, Integer age, Integer marks, Integer year);
   /** 
    * This is the method to be used to list down
    * all the records from the Student and Marks tables.
     * @return 
    */
   public List<StudentMarks> listStudents();
}
