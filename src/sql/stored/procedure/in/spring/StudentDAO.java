/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql.stored.procedure.in.spring;

import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author bumerrrang
 */
// https://www.tutorialspoint.com/spring/calling_stored_procedure.htm
// added additional netbean 8.1 library to compile and execute code: ~/netbeans-8.1/ide/modules/ext/mysql-connector-java-5.1.23-bin.jar
public interface StudentDAO {
   /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
     * @param ds
    */
   public void setDataSource(DataSource ds);
   /** 
    * This is the method to be used to create
    * a record in the Student table.
     * @param name
     * @param age
    */
   public void create(String name, Integer age);
   /** 
    * This is the method to be used to list down
    * a record from the Student table corresponding
    * to a passed student id.
    */
   public Student getStudent(Integer id);
   /** 
    * This is the method to be used to list down
    * all the records from the Student table.
     * @return 
    */
   public List<Student> listStudents();    
}
