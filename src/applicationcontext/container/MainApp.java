/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package applicationcontext.container;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/*
 * @author    : bumerang    
 * Document   : MainApp
 * Created on : Mar 1, 2017, 5:02:17 PM
 */
// https://www.tutorialspoint.com/spring/spring_applicationcontext_container.htm
public class MainApp {
   public static void main(String[] args) {

      ApplicationContext context = new FileSystemXmlApplicationContext
//            ("C:/Users/ZARA/workspace/HelloSpring/src/Beans.xml");
              ("/src/Beans.xml");

      HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
      obj.getMessage();
   }
}
