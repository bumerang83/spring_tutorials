/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package applicationcontext.container;

/*
 * @author    : bumerang    
 * Document   : HelloWorld
 * Created on : Mar 1, 2017, 5:01:41 PM
 */
// https://www.tutorialspoint.com/spring/spring_applicationcontext_container.htm
public class HelloWorld {
   private String message;

   public void setMessage(String message){
      this.message  = message;
   }

   public void getMessage(){
      System.out.println("Your Message : " + message);
   }
}
